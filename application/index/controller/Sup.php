<?php
namespace app\index\controller;
use think\View;
use think\Controller;
use app\index\model\User;
use think\Db;
/**
 *
 */
class Sup extends Controller{
  public function index(){
  	
    return $this->view->fetch('index');
  }

  public function sup($user_name='',$user_passwd=''){
  	// $user = User::get([
  	// 	'user_name' => $user_name,
  	// 	'user_passwd' => $user_passwd,
  	// 	]);

    $data = array();
    $data['user_name'] = $user_name;
    $data['user_passwd'] = $user_passwd;


    //这里把用户放在cookies里面，也就是放在会话里面，一遍后面拿出来。
    $user = Db::name('sup')->field('user_uid, user_name, user_email')->where($data)->find();
 
  	if($user){
      foreach($user as $k => $v){
        session(strtolower($k), strtolower($v));
      }
      // echo "</br>wssiwiwi</br>";
      $this->redirect('user/Vendingsup/index');

  	}else{
      return $this->error('登录失败');
  	}

  }
  
      public function logout(){
        session('memberinfo',null);
        session(null);
        $this->success('退出登录成功，跳转中……','index/Sup/');
    }  
  
}