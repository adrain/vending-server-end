<?php
namespace app\user\controller;

use think\Db;
use think\Controller;
use app\user\model;
use think\Log;
use think\Config;


class Vendingsup extends Base{
	public function _initialize(){
		parent::_initialize();
		$this->model = model('ItemType');
	}

	public function index(){
		//todo markmark to be continue现在已经吧add vendor 做完了，现在是，吧vendor，在user的主界面，显示出来。
        // $info = Db::name('user')->where(['user_uid' => session('user_uid')])->find();
        // $auto_id = input('param.auto_id','');
        
		$list = Db::name('item_type')->select();
		$this->assign('list', $list);  
		return $this->view->fetch('index');
        
	}
	public function userList(){
		
		$userList = Db::name('user')->where(['status'=> "1"])->select();
		$this->assign('userList', $userList);
		trace($userList,"info");
		
		return $this->view->fetch('userList');	
		
	}

    public function editItemType(){
        // $vendorUid = input('param.vendor_uid');
        // $info = $this->model->where("uid='{$vendorUid}'")->find();
        // $this->assign('info', $info);
        $itemTypeUid = input('param.item_type_uid');

        $info = $this->model->where("uid='{$itemTypeUid}'")->find();

        $this->assign('info', $info);

        return $this->view->fetch('vendingsup/edit_item_type');
    }

    public function saveInfo(){        
        $file = null;
        $fileInfo = null;
        $uid = input('post.uid');
        $info = $this->getDataByUid($uid);

        $data = array();
        $data['uid'] = $uid;
        $data["name"] = input('post.name');
        $file = request()->file('image');  
		$data['status'] = 1;

        //todo 这里把原来的image file name 查询出来。

        //编辑item的时候，判断是否上传新图片，如果是，就在要保存的data里面加上图片信息
        trace("5555","info");
		//$data["img"] = $info->img;
        //$data["file_name"] = $info->file_name;
		trace("5555","info");
        if($file){
            $itemTypeImgPath = Config::get("upload_paths")['itemtypes'];  
            $fileInfo = $file->move(ROOT_PATH . $itemTypeImgPath);
            $data["img"] = $itemTypeImgPath . DS . $fileInfo->getSaveName();
            $data["file_name"] = $file->getInfo('name');
        }


        //替换图片的时候，把原来的图片删掉。
        if($data['uid'] != ""){
            if($file){
                $realPath = ROOT_PATH . $info->img;
               
                if(file_exists($realPath)){
                    
                    unlink($realPath);    
                }
            }

            $result = $this->model->where('uid', $data['uid'])->update($data);

        }else{

            if($fileInfo){
                $data["uid"] = uniqid();
                $result = $this->model->data($data)->save();
            } 
        }

        $this->redirect("user/vendingsup/index");
    }

    public function saveItemType(){

    }
	
	
    public function removeinfo(){
	    parent::_initialize();
		$this->model = model('ItemType');	
    	// $auto_id = input('param.auto_id');
        $uid = input('param.uid');

        //todo 这里判断一下， 这个item type 是否已经有商家在使用了。

        $queryResult = Db::name("goods")->where("item_type_uid='{$uid}'")->find();


        // $info = $this->model->where("uid='{$vendorUid}'")->find();
        // $list = Db::name('vendor')->where(['user_uid'=> session('user_uid')])->select();
        $result = false;
        if(!$queryResult){

            $result = $this->model->where('uid',$uid)->delete();
        }
    	
        // if(false !== $result){
        if($result and !$queryResult){
        $itemstatus = Db::table("think_item_type")->where('uid',0)->update(['status' => 1]);			
    		$data = [
    			'status' => 0,
    			'msg' => '删除成功！',
    		];
    	}else{
    		$data = [
    			'status' => 1,
    			'msg' => '删除失败！可能被改商品种类已经被使用当中',
    		];
    	}
    	echo json_encode($data);
    }
}