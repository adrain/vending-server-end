<?php
namespace app\user\controller;

use think\Db;
use think\Controller;
use app\user\model;
use think\Log;
use think\Config;


class Vending extends Base{
	public function _initialize(){
		parent::_initialize();
		$this->model = model('Vendor');
	}

	public function index(){
		$list = Db::name('vendor')->where(['user_uid'=> session('user_uid')])->order('auto_id', 'asc')->select();
        trace($list,"info");
		//trace($list[0]["user_uid"],"info");


		$this->assign('list', $list);  
		return $this->view->fetch('index1');
	}
	
	public function indexadd(){
		$addlist = Db::name('ImgType')->where(['user_uid'=> session('user_uid')])->select();
		$this->assign('addlist', $addlist);  
		return $this->view->fetch('indexadd');
	}
	
    public function trans(){
		$vendorUid = input('param.vendor_uid');
		trace($vendorUid,"info");
			
		$addlist = Db::name('vendor')->where(['user_uid'=> session('user_uid')])->select();
		$this->assign('addlist', $addlist);

		$addlist2 = Db::name('goods')->where(['vendor_uid'=>$vendorUid])->select();
		$this->assign('addlist2', $addlist2);
		
		$addlist3 = Db::name('vendor')->where(['uid'=>$vendorUid ])->select();
		$this->assign('addlist3', $addlist3);		
		
		
		$addlist1 = Db::name('ItemType')->where(['uid'=>'5b1df1045fa8f'])->select();
		$this->assign('addlist1', $addlist1);
		
        $list = Db::table("think_goods")->alias('g')->join('item_type i', 'g.item_type_uid=i.uid')->field('g.*, i.img, i.name, i.file_name')->select();

        //to be continue 这里的uid指的是item type uid的uid，我希望两个都看到，但是两个字段是一样的
        ///最后只是显示其中一个。

        $this->assign('list', $list);		

        trace($addlist2,"info");		
		return $this->view->fetch('trans');
	}
	
	
	public function trade(){
		$vendorUid = input('param.vendor_uid');
		$paydata = Db::name('pay')->where(['status'=>'0'])->select();		
		$addlist = Db::name('vendor')->where(['user_uid'=> session('user_uid')])->select();
		$this->assign('addlist', $addlist);
		$addlist2 = Db::name('goods')->where(['vendor_uid'=>$vendorUid])->select();
		$this->assign('addlist2', $addlist2);		
		$addlist3 = Db::name('vendor')->where(['uid'=>$vendorUid ])->select();
		$this->assign('addlist3', $addlist3);				
		$addlist1 = Db::name('ItemType')->where(['uid'=>'5b1df1045fa8f'])->select();
		$this->assign('addlist1', $addlist1);
		
        $list = Db::table("think_goods")->alias('g')->join('item_type i', 'g.item_type_uid=i.uid')->field('g.*, i.img, i.name, i.file_name')->select();

        //to be continue 这里的uid指的是item type uid的uid，我希望两个都看到，但是两个字段是一样的
        ///最后只是显示其中一个。
        $this->assign('list', $list);
		$paydata = Db::name('pay')->where(['status'=>'0'])->select();
        $this->assign('paylist',$paydata);		
		//trace($list,"info");
		$json = json_encode($list);
		trace($json,"info");
		$newjson=('{"data":'.$json.'}');
		trace($newjson,"info");
		return $this->view->fetch('pay');
	}
	
	public function trade_pay($vendor_uid){
		$vendorUid = input('param.vendor_uid');
		$paydata = Db::name('pay')->where(['status'=>'0'])->select();		
		$addlist = Db::name('vendor')->where(['user_uid'=> session('user_uid')])->select();
		$this->assign('addlist', $addlist);
		$addlist2 = Db::name('goods')->where(['vendor_uid'=>$vendorUid])->select();
		$this->assign('addlist2', $addlist2);		
		$addlist3 = Db::name('vendor')->where(['uid'=>$vendorUid ])->select();
		$this->assign('addlist3', $addlist3);				
		$addlist1 = Db::name('ItemType')->where(['uid'=>'5b1df1045fa8f'])->select();
		$this->assign('addlist1', $addlist1);		
        $list = Db::table("think_goods")->alias('g')->join('item_type i', 'g.item_type_uid=i.uid')->field('g.*, i.img, i.name, i.file_name')->select();

        //to be continue 这里的uid指的是item type uid的uid，我希望两个都看到，但是两个字段是一样的
        ///最后只是显示其中一个。
        $this->assign('list', $list);
		$paydata = Db::name('pay')->where(['vendor_uid'=>$vendor_uid])->select();
        $this->assign('paylist',$paydata);
        $vendername = Db::name('vendor')->where(['uid'=>$vendor_uid])->select();
		$this->assign('vendername',$vendername);
		trace($vendername,"info");
		$json = json_encode($list);
		trace($paydata,"info");
		trace($json,"info");
		$newjson=('{"data":'.$json.'}');
		trace($newjson,"info");
		return $this->view->fetch('pay');
	}
	
	public function get_date($date){
		
		
		echo $date;
		$vendorstatus = Db::table("think_pay")->where('good_uid',"5b3e121b288af")->update(['pay_time' => $date ]);
		echo "111111";	

	}	
	
    public function data(){
        $list = Db::table("think_goods")->alias('g')->join('item_type i', 'g.item_type_uid=i.uid')->field('g.*, i.img, i.name, i.file_name')->select();
        trace($list,"info");
		$paydata = Db::name('pay')->where(['status'=>'0'])->select();
		trace($paydata,"info");			
		$json = json_encode($paydata);
		echo ('{"data":'.$json.'}');
			
		}
	
	

	public function editVendor(){
		$vendorUid = input('param.vendor_uid');
		$info = $this->model->where("uid='{$vendorUid}'")->find();


		$this->assign('info', $info);
		trace($info,"info");

		return $this->view->fetch('edit_vendor');
	}
	
	public function editImgType(){
        // $vendorUid = input('param.vendor_uid');
        // $info = $this->model->where("uid='{$vendorUid}'")->find();
        // $this->assign('info', $info);
		parent::_initialize();
		$this->model = model('ImgType');
		$list = Db::name('img_type')->select();
		$this->assign('list', $list); 		
        $imgTypeUid = input('param.img_type_uid');
        
        $info = $this->model->where("uid='{$imgTypeUid}'")->find();
		
        $this->assign('info', $info);

        return $this->view->fetch('vending/edit_img_type');
    }
	
	public function saveInfo1(){
		parent::_initialize();
		$this->model = model('ImgType');
		trace("11","info");
		$list = Db::name('img_type')->select(); 				
        $file = null;
        $fileInfo = null;
        $uid = input('post.uid');
        $info = $this->getDataByUid($uid);		
        $data = array();
        $data['uid'] = $uid;
        $data["name"] = input('post.name');
		$data['status'] = 1;
		$data['ex'] =0;
		//$data["user_uid"] = input();
        $file = request()->file('image'); 
        trace("22","info");		


        //编辑item的时候，判断是否上传新图片，如果是，就在要保存的data里面加上图片信息
        if($file){
            $imgTypeImgPath = Config::get("upload_paths")['imgtypes'];  
            $fileInfo = $file->move(ROOT_PATH . $imgTypeImgPath);
            $data["img"] = $imgTypeImgPath . DS . $fileInfo->getSaveName();
            $data["file_name"] = $file->getInfo('name');
			trace("33","info");
		    $list = Db::name('vendor')->where(['user_uid'=> session('user_uid')])->select();
		    // trace($list[0]["user_uid"],"info");			
			$addme=$list[0]["user_uid"] ;
			$data["user_uid"]=$addme;
			//trace($addme,"info");
			//trace($data,"info");
			
        }


        //替换图片的时候，把原来的图片删掉。
        if($data['uid'] != ""){
            if($file){
                $realPath = ROOT_PATH . $info->img;
				trace($realPath,"info");
               
                if(file_exists($realPath)){
                    
                    unlink($realPath);    
                }
            }

            $result = $this->model->where('uid', $data['uid'])->update($data);
        }else{

            if($fileInfo){
                $data["uid"] = uniqid();
                $result = $this->model->data($data)->save();
            } 
        }

        $this->redirect("user/vending/indexadd");
    }


	//增加，和修改，都能
	public function saveVendor(){
		//todo markmark 这里加数据合法性验证，使用contorller的validate函数，自己定义验证内容
        $info = Db::name('user')->where(['user_uid' => session('user_uid')])->find();
        if($info){
			$data = input('post.');
	        $data['user_uid'] = $info['user_uid'];
			$data['status_vendor'] = 1; 

	  //       //这里判断究竟是add还是update
	        if($data['uid'] != ""){

	        	$result = $this->model->where('uid', $data['uid'])->update($data);
	        }else{
	        	$data['uid'] = uniqid();

	        	$result = $this->model->data($data)->save();
	        }
        }

    	// return $this->fetch('index');
		$this->redirect('user/Vending/index');

	}



    public function removeinfo(){
	    parent::_initialize();
		$this->model = model('ImgType');	
    	$auto_id = input('param.auto_id');
		
		trace($auto_id,"info");
		
		$addlist = Db::name('ImgType')->where("auto_id='{$auto_id}'")->select();
			
                /*if(file_exists($realPath)){
                    
                    unlink($realPath);    
                }	*/	
		
		
		
    	$result = $this->model->where('auto_id',$auto_id)->delete();
    	if(false !== $result){
			$imgstatus = Db::table("think_img_type")->where('uid','0')->update(['status' => 1]);
    		$data = [
    			'status' => 0,
    			'msg' => '删除成功！',
    		];
    	}else {
    		$data = [
    			'status' => 1,
    			'msg' => '删除失败！',
    		];
    	}
    	echo json_encode($data);
    }
	
	public function addVendor1(){
		echo "1111";
		return $this->view->fetch('chart_xy');
	}

	public function alipay(){

		return $this->view->fetch('Alipay_QRCode');
	}
	
		public function addVendor2(){
		echo "11111111111111";
		return $this->view->fetch('sup');
	}
	
	
	public function payment(){
	
	require 'C:\wamp\011\vendor\AlipaySdk\wappay\service\AlipayTradeService.php';
	require 'C:\wamp\011\vendor\AlipaySdk\wappay\buildermodel/AlipayTradeWapPayContentBuilder.php';	

    if (!empty($_POST['WIDout_trade_no'])&& trim($_POST['WIDout_trade_no'])!=""){
        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = $_POST['WIDout_trade_no'];

        //订单名称，必填
        $subject = $_POST['WIDsubject'];

        //付款金额，必填
        $total_amount = $_POST['WIDtotal_amount'];

        //商品描述，可空
        $body = $_POST['WIDbody'];

        //超时时间
        $timeout_express="1m";
    	
    	trace($out_trade_no,"info");

        $payRequestBuilder = new AlipayTradeWapPayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setOutTradeNo($out_trade_no);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setTimeExpress($timeout_express);
        $config = Config::get("alipay");
        trace($payRequestBuilder,"info");
    	trace($config,"info");
    	echo "tesr";
    	
    	$payResponse = new AlipayTradeService($config);
    	
    	$result=$payResponse->wapPay($payRequestBuilder,$config['return_url'],$config['notify_url']);
    	trace($result,"info");
    	
        return $this->view->fetch('alipaybak');
    }
	echo "ssss";
	
		return $this->view->fetch('alipaybak');
	}


	public function stream(){
    	$streamlist = Db::name('stream')->select();

    	foreach($streamlist as $key=>$value){
    	$uidlist = Db::name('vendor')->where("uuid",$streamlist[$key]['uuid'])->find();
        $streamlist[$key]['uuid']= $uidlist["name"];	
        }
    	
    	trace($streamlist,"info11");
        $this->assign('streamlist',$streamlist);	
    	return $this->view->fetch('stream');			
    	}	
        public function apkupdate(){

    	$list = Db::name('apk')->find();
    	$realPath = $list['apk'];
        $this->assign('info',$list);
        //trace($realPath,"info");	
    	return $this->view->fetch('apk');			
	}

	public function upload(){
	    $list = Db::name('apk')->find();
	    $realPath = $list['apk'];	
		$data = array();
		$data['status'] = 1;
        $data["file_name"] = input('post.file_name');
        $ApkPath = Config::get("upload_paths")['apk'];  		
         // 获取表单上传文件		 
         $file = request()->file('files');         
         if (empty($file)) {
             $this->error('请选择上传文件');
         }
         // 移动到框架应用根目录/public/uploads/ 目录下
         $info = $file->move(ROOT_PATH . $ApkPath);
		 $data["apk"] = $ApkPath . DS . $info->getSaveName();
		 $result = Db::name('apk')->where('auto_id', 1)->update($data);
		 trace($data,"info");	 
         if ($info) {
			 unlink(ROOT_PATH .$realPath); 
             $this->success('文件上传成功');
             echo $info->getFilename();
         } else {
             // 上传失败获取错误信息
             $this->error($file->getError());
         }	
	 }
}